import React from 'react';
import logo from './logo.svg';
import './App.css';
import StockViewer from './components/StockViewer/StockViewer';

function App() {
  return (
    <div className="App">
      <StockViewer></StockViewer>
    </div>
  );
}

export default App;
